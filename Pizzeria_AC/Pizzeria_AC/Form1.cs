﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pizzeria_AC
{
    public partial class Form1 : Form
    {

        int[] tamanios = new int[3];
        int[] masas = new int[4];
        int[] extras = new int[8];

        public Form1()
        {
            InitializeComponent();

            tamanios[0] = 7;
            tamanios[1] = 11;
            tamanios[2] = 16;

            masas[0] = 2;
            masas[1] = 1;
            masas[2] = 3;
            masas[3] = 4;

            extras[0] = 1;
            extras[1] = 1;
            extras[2] = 1;
            extras[3] = 1;
            extras[5] = 1;
            extras[6] = 1;
            extras[7] = 1;
        }

        public void deshacer()
        {
            foreach (int checkedItemIndex in checkedListBoxPrincipales.CheckedIndices)
            {
                checkedListBoxPrincipales.SetItemChecked(checkedItemIndex, false);
            }

            foreach (int checkedItemIndex in checkedListBoxSecundarios.CheckedIndices)
            {
                checkedListBoxSecundarios.SetItemChecked(checkedItemIndex, false);
            }

            foreach (int checkedItemIndex in checkedListBoxTamano.CheckedIndices)
            {
                checkedListBoxTamano.SetItemChecked(checkedItemIndex, false);
            }
            foreach (int checkedItemIndex in checkedListBoxMasa.CheckedIndices)
            {
                checkedListBoxMasa.SetItemChecked(checkedItemIndex, false);
            }
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            deshacer();
            checkedListBoxSecundarios.Enabled = true;
            checkedListBoxMasa.Enabled = true;
            checkedListBoxPrincipales.Enabled = true;
            checkedListBoxTamano.Enabled = true;

                if (comboBox1.SelectedIndex == 0)
                {
                deshacer();
                    checkedListBoxPrincipales.SetItemChecked(0, true);
                    checkedListBoxPrincipales.SetItemChecked(1, true);       
                }

                if (comboBox1.SelectedIndex == 1)
                {
                deshacer();
                    checkedListBoxPrincipales.SetItemChecked(2, true);
                    checkedListBoxPrincipales.SetItemChecked(3, true);
                    checkedListBoxTamano.SetItemChecked(0, true);

                }

                if (comboBox1.SelectedIndex == 2)
                {
                deshacer();
                    checkedListBoxPrincipales.SetItemChecked(4, true);
                    checkedListBoxPrincipales.SetItemChecked(5, true);

                }

                if (comboBox1.SelectedIndex == 3)
                {
                deshacer();
                    for (int i = 6; i <= 9; i++)
                    {
                        checkedListBoxPrincipales.SetItemChecked(i, true);
                }
                checkedListBoxPrincipales.Enabled = false;
                checkedListBoxSecundarios.Enabled = false;
                checkedListBoxMasa.Enabled = false;
                    checkedListBoxMasa.SetItemChecked(0, true);
                }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cuenta();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string mensage = "¿Realmente desea salir ?";
            string titulo = "Salir";
            MessageBoxButtons opciones = MessageBoxButtons.OK;
            DialogResult result = MessageBox.Show("No has elegido nada", "Menu");
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                Application.Exit();
            }
        }
    
        private void Extras()
        {
            int resultado = 0;

            if (comboBox1.SelectedIndex == 3)
            {
                resultado += 0;
            }
            else
            {
                    resultado += extras[checkedListBoxSecundarios.SelectedIndex];
            }
        }

        private void Cuenta()
        {
            int resultado = 0;

            if (checkedListBoxTamano.CheckedIndices.Count == 0 || checkedListBoxMasa.CheckedIndices.Count == 0)
            {

                MessageBoxButtons buttons2 = MessageBoxButtons.OK;
                DialogResult = MessageBox.Show("falta elegir algo en una categoria", "Select", buttons2, MessageBoxIcon.Warning);
            }
            else
            {
                resultado += extras[checkedListBoxSecundarios.SelectedIndex];
                resultado += tamanios[checkedListBoxTamano.SelectedIndex];
                resultado += masas[checkedListBoxMasa.SelectedIndex+1];
                Extras();

                MessageBoxButtons buttons2 = MessageBoxButtons.OK;
                DialogResult = MessageBox.Show("La factura es: "+ resultado, "Factura",buttons2, MessageBoxIcon.Exclamation);

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string[] IngredienteSecun = new string[checkedListBoxSecundarios.Items.Count];
            string[] ingredientesPrinci = new string[checkedListBoxPrincipales.Items.Count];
            string[] Tamanio = new string[checkedListBoxTamano.Items.Count];
            string[] Masa = new string[checkedListBoxMasa.Items.Count];

            MessageBoxButtons buttons = MessageBoxButtons.OK;

            richTextBox1.Text = "";
            richTextBox1.Text += "La pizza elegida es: " + comboBox1.SelectedItem + "\n\r";

            richTextBox1.Text += "Tipo de ingredientes: " + "\n";
            for (int i = 0; i < checkedListBoxPrincipales.Items.Count; i++)
            {
                if ((checkedListBoxPrincipales.CheckedIndices.Count >= 2) && (checkedListBoxPrincipales.CheckedIndices.Count <= 4))
                {
                    if (checkedListBoxPrincipales.GetItemChecked(i))
                    {
                        ingredientesPrinci[i] = checkedListBoxPrincipales.Items[i].ToString();
                        richTextBox1.Text += ingredientesPrinci[i] + ",";
                    }
                }
                else
                {
                    i = 10; 
                    string mensage = "No has seleccionado ningun campo";
                    MessageBoxButtons opciones = MessageBoxButtons.OK;
                    DialogResult result = MessageBox.Show(mensage,"mesa", opciones, MessageBoxIcon.Error);
                }
            }

            richTextBox1.Text += "\n\n" + "Ingrediente Extra elegidos: " + "\n";

            for (int i = 0; i < checkedListBoxSecundarios.Items.Count; i++)
            {
                if (checkedListBoxSecundarios.GetItemChecked(i))
                {
                    IngredienteSecun[i] = checkedListBoxSecundarios.Items[i].ToString();
                    richTextBox1.Text += IngredienteSecun[i] + " ";
                }
            }

            richTextBox1.Text += "\n\n" + "Tamaño Pizza:" + "\n";

            for (int i=0; i<checkedListBoxTamano.Items.Count; i++)
            {
                if(checkedListBoxTamano.GetItemChecked(i))
                {
                    Tamanio[i] = checkedListBoxTamano.Items[i].ToString();
                    richTextBox1.Text += Tamanio[i] + " ";
                }
            }

            richTextBox1.Text += "\n\n" + "Masa de la pizza:" + "\n";
            for (int i = 0; i < checkedListBoxMasa.Items.Count; i++)
            {
                if (checkedListBoxMasa.GetItemChecked(i))
                {
                    Masa[i] = checkedListBoxMasa.Items[i].ToString();
                    richTextBox1.Text += Masa[i] + " ";
                }
            }

            if(checkedListBoxSecundarios.CheckedItems.Count <2)
            {
                if ((comboBox1.SelectedIndex == 0) || (comboBox1.SelectedIndex == 2))
                {
                    DialogResult = MessageBox.Show("Debe elegir como mínimo 2 extras para esta pizza", "Extras", buttons, MessageBoxIcon.Information);

                }
            }

            if (checkedListBoxSecundarios.CheckedItems.Count > 4)
            {
                if ((comboBox1.SelectedIndex == 0) || (comboBox1.SelectedIndex == 2))
                {
                    DialogResult = MessageBox.Show("Debe elegir como maximo 4 extras para esta pizza", "Extras", buttons, MessageBoxIcon.Information);

                }
            }

            if (checkedListBoxTamano.CheckedItems.Count == 0)
            {
                DialogResult = MessageBox.Show("Seleccionar tipo de tamaño","Tamanio",buttons,MessageBoxIcon.Warning);
            }

            if(checkedListBoxMasa.CheckedItems.Count == 0)
            {
                DialogResult = MessageBox.Show("Seleccionar una masa", "Masa", buttons, MessageBoxIcon.Warning);
            }
        }

        private void checkedListBoxTamano_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (int i in checkedListBoxTamano.CheckedIndices)
                if (i != checkedListBoxTamano.SelectedIndex)
                    checkedListBoxTamano.SetItemChecked(i, false);
                else
                {
                    DialogResult = MessageBox.Show("Falta seleccionar un tamaño", "Tamaño");
                }
        }

        private void checkedListBoxMasa_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (int i in checkedListBoxMasa.CheckedIndices)
                if (i != checkedListBoxMasa.SelectedIndex)
                    checkedListBoxMasa.SetItemChecked(i, false);
                else
                {
                    DialogResult = MessageBox.Show("Falta seleccionar un tipo de masa", "Tamaño");
                }
        }
        public void vegariana()
        {
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            if (comboBox1.SelectedIndex == 1)
            {

                checkedListBoxSecundarios.GetItemChecked(0);
                checkedListBoxSecundarios.SetItemChecked(0, false);
                checkedListBoxSecundarios.GetItemChecked(1);
                checkedListBoxSecundarios.SetItemChecked(1, false);
                checkedListBoxSecundarios.GetItemChecked(3);
                checkedListBoxSecundarios.SetItemChecked(3, false);
                checkedListBoxSecundarios.GetItemChecked(6);
                checkedListBoxSecundarios.SetItemChecked(6, false);
                checkedListBoxSecundarios.GetItemChecked(7);
                checkedListBoxSecundarios.SetItemChecked(7, false);
                checkedListBoxSecundarios.GetItemChecked(8);
                checkedListBoxSecundarios.SetItemChecked(8, false);
                DialogResult = MessageBox.Show("En la Vegetariano no se pueden elegir ingredientes de animales", "vegetariana", buttons, MessageBoxIcon.Warning);
            }
            else
            {

            }
        }
        private void checkedListBoxSecundarios_SelectedIndexChanged(object sender, EventArgs e)
        {
            vegariana();
        }
    }
}

