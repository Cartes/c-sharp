﻿namespace _05_WindowsFormsApplicationClases
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCirculo1 = new System.Windows.Forms.Button();
            this.buttonCirculo2 = new System.Windows.Forms.Button();
            this.buttonCirculo3 = new System.Windows.Forms.Button();
            this.textBoxRadio2 = new System.Windows.Forms.TextBox();
            this.textBoxRadio1 = new System.Windows.Forms.TextBox();
            this.textBoxColor1 = new System.Windows.Forms.TextBox();
            this.textBoxColor2 = new System.Windows.Forms.TextBox();
            this.textBoxColor3 = new System.Windows.Forms.TextBox();
            this.textBoxRadio3 = new System.Windows.Forms.TextBox();
            this.labelResultado = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonCirculo1
            // 
            this.buttonCirculo1.Location = new System.Drawing.Point(12, 65);
            this.buttonCirculo1.Name = "buttonCirculo1";
            this.buttonCirculo1.Size = new System.Drawing.Size(158, 54);
            this.buttonCirculo1.TabIndex = 0;
            this.buttonCirculo1.Text = "button1";
            this.buttonCirculo1.UseVisualStyleBackColor = true;
            this.buttonCirculo1.Click += new System.EventHandler(this.buttonCirculo1_Click);
            // 
            // buttonCirculo2
            // 
            this.buttonCirculo2.Location = new System.Drawing.Point(226, 66);
            this.buttonCirculo2.Name = "buttonCirculo2";
            this.buttonCirculo2.Size = new System.Drawing.Size(158, 53);
            this.buttonCirculo2.TabIndex = 1;
            this.buttonCirculo2.Text = "button2";
            this.buttonCirculo2.UseVisualStyleBackColor = true;
            // 
            // buttonCirculo3
            // 
            this.buttonCirculo3.Location = new System.Drawing.Point(422, 64);
            this.buttonCirculo3.Name = "buttonCirculo3";
            this.buttonCirculo3.Size = new System.Drawing.Size(158, 54);
            this.buttonCirculo3.TabIndex = 2;
            this.buttonCirculo3.Text = "button3";
            this.buttonCirculo3.UseVisualStyleBackColor = true;
            this.buttonCirculo3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBoxRadio2
            // 
            this.textBoxRadio2.Location = new System.Drawing.Point(226, 12);
            this.textBoxRadio2.Name = "textBoxRadio2";
            this.textBoxRadio2.Size = new System.Drawing.Size(158, 20);
            this.textBoxRadio2.TabIndex = 3;
            this.textBoxRadio2.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBoxRadio1
            // 
            this.textBoxRadio1.Location = new System.Drawing.Point(12, 12);
            this.textBoxRadio1.Name = "textBoxRadio1";
            this.textBoxRadio1.Size = new System.Drawing.Size(158, 20);
            this.textBoxRadio1.TabIndex = 3;
            this.textBoxRadio1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBoxColor1
            // 
            this.textBoxColor1.Location = new System.Drawing.Point(12, 39);
            this.textBoxColor1.Name = "textBoxColor1";
            this.textBoxColor1.Size = new System.Drawing.Size(158, 20);
            this.textBoxColor1.TabIndex = 3;
            this.textBoxColor1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBoxColor2
            // 
            this.textBoxColor2.Location = new System.Drawing.Point(226, 38);
            this.textBoxColor2.Name = "textBoxColor2";
            this.textBoxColor2.Size = new System.Drawing.Size(158, 20);
            this.textBoxColor2.TabIndex = 3;
            this.textBoxColor2.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBoxColor3
            // 
            this.textBoxColor3.Location = new System.Drawing.Point(422, 38);
            this.textBoxColor3.Name = "textBoxColor3";
            this.textBoxColor3.Size = new System.Drawing.Size(158, 20);
            this.textBoxColor3.TabIndex = 3;
            this.textBoxColor3.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBoxRadio3
            // 
            this.textBoxRadio3.Location = new System.Drawing.Point(422, 12);
            this.textBoxRadio3.Name = "textBoxRadio3";
            this.textBoxRadio3.Size = new System.Drawing.Size(158, 20);
            this.textBoxRadio3.TabIndex = 3;
            this.textBoxRadio3.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // labelResultado
            // 
            this.labelResultado.AutoSize = true;
            this.labelResultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultado.Location = new System.Drawing.Point(12, 191);
            this.labelResultado.Name = "labelResultado";
            this.labelResultado.Size = new System.Drawing.Size(237, 37);
            this.labelResultado.TabIndex = 4;
            this.labelResultado.Text = "El resultado es:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(688, 342);
            this.Controls.Add(this.labelResultado);
            this.Controls.Add(this.textBoxRadio1);
            this.Controls.Add(this.textBoxColor1);
            this.Controls.Add(this.textBoxColor3);
            this.Controls.Add(this.textBoxRadio3);
            this.Controls.Add(this.textBoxColor2);
            this.Controls.Add(this.textBoxRadio2);
            this.Controls.Add(this.buttonCirculo3);
            this.Controls.Add(this.buttonCirculo2);
            this.Controls.Add(this.buttonCirculo1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCirculo1;
        private System.Windows.Forms.Button buttonCirculo2;
        private System.Windows.Forms.Button buttonCirculo3;
        private System.Windows.Forms.TextBox textBoxRadio2;
        private System.Windows.Forms.TextBox textBoxRadio1;
        private System.Windows.Forms.TextBox textBoxColor1;
        private System.Windows.Forms.TextBox textBoxColor2;
        private System.Windows.Forms.TextBox textBoxColor3;
        private System.Windows.Forms.TextBox textBoxRadio3;
        private System.Windows.Forms.Label labelResultado;
    }
}

