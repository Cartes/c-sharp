﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{ 
    public enum EstadoMesa {Libre, Reservada, Ocupada}; 

    class Mesa
    {
        private int nOcupantesMax, nOcupantesAct;
        private EstadoMesa estado;
        private string horaOcupacion;

        private float caja;

        //Constructores
        public Mesa()
        {
            nOcupantesMax = 4;
            nOcupantesAct = 0;
            estado = EstadoMesa.Libre;
            horaOcupacion = "";
            caja = 0;
        }

        public Mesa(int nOcMax, int nOcAct, EstadoMesa e, string horaR, float Ncaja)
        {
            nOcupantesMax = nOcMax;
            nOcupantesAct = nOcAct;
            estado = e;
            horaOcupacion = horaR;
            caja = Ncaja;
        }

        //Ocupar

        public void Ocupar(int nOcupantes, string horaO)
        {
                if ((estado == EstadoMesa.Libre) && (nOcupantes <= nOcupantesMax))
                {
                estado = EstadoMesa.Ocupada;
                nOcupantesAct = nOcupantes;
                horaOcupacion = horaO;
                }
            }

        //Cobrar

        public void Cobrar()
        {
            if (estado == EstadoMesa.Ocupada)
            {

                estado = EstadoMesa.Libre;
                caja += 10 * nOcupantesAct;
                horaOcupacion = "";
            }


        }

        public void libres()
        {
            int resultado;
            resultado = nOcupantesAct - nOcupantesMax;
        }

        public int getMaxmesa()
        {
            return nOcupantesMax;
        }

        public int getOcupadas()
        {
            return nOcupantesAct;
        }



    }
}
