﻿namespace WindowsFormsApplication_Restaurante
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.labelOcupantesMaximo = new System.Windows.Forms.Label();
            this.labelOcupantesActuales = new System.Windows.Forms.Label();
            this.labelEstadoMesa = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(34, 54);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(118, 51);
            this.button1.TabIndex = 0;
            this.button1.Text = "Mesa 1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // labelOcupantesMaximo
            // 
            this.labelOcupantesMaximo.AutoSize = true;
            this.labelOcupantesMaximo.Location = new System.Drawing.Point(172, 55);
            this.labelOcupantesMaximo.Name = "labelOcupantesMaximo";
            this.labelOcupantesMaximo.Size = new System.Drawing.Size(35, 13);
            this.labelOcupantesMaximo.TabIndex = 1;
            this.labelOcupantesMaximo.Text = "label1";
            // 
            // labelOcupantesActuales
            // 
            this.labelOcupantesActuales.AutoSize = true;
            this.labelOcupantesActuales.Location = new System.Drawing.Point(172, 72);
            this.labelOcupantesActuales.Name = "labelOcupantesActuales";
            this.labelOcupantesActuales.Size = new System.Drawing.Size(35, 13);
            this.labelOcupantesActuales.TabIndex = 2;
            this.labelOcupantesActuales.Text = "label1";
            // 
            // labelEstadoMesa
            // 
            this.labelEstadoMesa.AutoSize = true;
            this.labelEstadoMesa.Location = new System.Drawing.Point(172, 92);
            this.labelEstadoMesa.Name = "labelEstadoMesa";
            this.labelEstadoMesa.Size = new System.Drawing.Size(35, 13);
            this.labelEstadoMesa.TabIndex = 3;
            this.labelEstadoMesa.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 469);
            this.Controls.Add(this.labelEstadoMesa);
            this.Controls.Add(this.labelOcupantesActuales);
            this.Controls.Add(this.labelOcupantesMaximo);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label labelOcupantesMaximo;
        private System.Windows.Forms.Label labelOcupantesActuales;
        private System.Windows.Forms.Label labelEstadoMesa;
    }
}

