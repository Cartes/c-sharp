﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _04_WindowsFormsApplicationCadenas
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonInicializar_Click(object sender, EventArgs e)
        {
            textBoxCadena.Text = "";
        }

        private void buttonEscribir_Click(object sender, EventArgs e)
        {
            labelResultado.Text = "La cadena es: " + textBoxCadena.Text;
        }

        private void buttonInvertir_Click(object sender, EventArgs e)
        {
            string invertir = "";

            for (int posicion = textBoxCadena.Text.Length - 1; posicion >= 0; posicion--)
            {
                invertir += textBoxCadena.Text.Substring(posicion, 1);
            }
            labelResultado.Text = invertir;
        }

        private void buttonMayus_Click(object sender, EventArgs e)
        {
           //  labelResultado.Text = textBoxCadena.Text.ToUpper();

            //'A' ---> 'Z' 'a' --- 'z'

            char[] tabla = textBoxCadena.Text.ToCharArray();
            string cadena = "";
            for (int i=0; i<tabla.Length; i++)
            {
                if ((tabla[i] >= 'a') && (tabla[i] <= 'z'))
                {
                    cadena += (char)(tabla[i] - 32);
                }
                else
                    cadena += tabla[i];
            }
            labelResultado.Text = cadena;
         }

        private void buttonMinusc_Click(object sender, EventArgs e)
        {
         //   labelResultado.Text = textBoxCadena.Text.ToLower();


            char[] tabla = textBoxCadena.Text.ToCharArray();
            string cadena = "";
            for (int i = 0; i < tabla.Length; i++)
            {
                if ((tabla[i] >= 'A') && (tabla[i] <= 'Z'))
                {
                    cadena += (char)(tabla[i] + 32);
                }
                else
                    cadena += tabla[i];
            }
            labelResultado.Text = cadena;
        }

        private void buttonRotarD_Click(object sender, EventArgs e)
        {
            string cadena = textBoxCadena.Text;
            labelResultado.Text = cadena.Substring(cadena.Length - 1) +
            cadena.Remove(cadena.Length - 1);

            textBoxCadena.Text = labelResultado.Text;
            
        }

        private void buttonRotarI_Click(object sender, EventArgs e)
        {
            string cadena = textBoxCadena.Text;
            string temporal = cadena[0].ToString();

            cadena = cadena.Remove(0, 1);
            cadena += temporal;
            labelResultado.Text = cadena;
        }
    }
    }

