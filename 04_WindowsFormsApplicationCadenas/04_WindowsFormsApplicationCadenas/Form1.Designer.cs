﻿namespace _04_WindowsFormsApplicationCadenas
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxCadena = new System.Windows.Forms.TextBox();
            this.labelResultado = new System.Windows.Forms.Label();
            this.buttonInicializar = new System.Windows.Forms.Button();
            this.buttonEscribir = new System.Windows.Forms.Button();
            this.buttonInvertir = new System.Windows.Forms.Button();
            this.buttonMayus = new System.Windows.Forms.Button();
            this.buttonMinusc = new System.Windows.Forms.Button();
            this.buttonRotarD = new System.Windows.Forms.Button();
            this.buttonRotarI = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxCadena
            // 
            this.textBoxCadena.Location = new System.Drawing.Point(12, 50);
            this.textBoxCadena.Name = "textBoxCadena";
            this.textBoxCadena.Size = new System.Drawing.Size(169, 20);
            this.textBoxCadena.TabIndex = 0;
            // 
            // labelResultado
            // 
            this.labelResultado.AutoSize = true;
            this.labelResultado.Location = new System.Drawing.Point(210, 50);
            this.labelResultado.Name = "labelResultado";
            this.labelResultado.Size = new System.Drawing.Size(35, 13);
            this.labelResultado.TabIndex = 1;
            this.labelResultado.Text = "label1";
            // 
            // buttonInicializar
            // 
            this.buttonInicializar.Location = new System.Drawing.Point(2, 88);
            this.buttonInicializar.Name = "buttonInicializar";
            this.buttonInicializar.Size = new System.Drawing.Size(105, 33);
            this.buttonInicializar.TabIndex = 2;
            this.buttonInicializar.Text = "Inicializar";
            this.buttonInicializar.UseVisualStyleBackColor = true;
            this.buttonInicializar.Click += new System.EventHandler(this.buttonInicializar_Click);
            // 
            // buttonEscribir
            // 
            this.buttonEscribir.Location = new System.Drawing.Point(113, 89);
            this.buttonEscribir.Name = "buttonEscribir";
            this.buttonEscribir.Size = new System.Drawing.Size(107, 33);
            this.buttonEscribir.TabIndex = 3;
            this.buttonEscribir.Text = "Escribir";
            this.buttonEscribir.UseVisualStyleBackColor = true;
            this.buttonEscribir.Click += new System.EventHandler(this.buttonEscribir_Click);
            // 
            // buttonInvertir
            // 
            this.buttonInvertir.Location = new System.Drawing.Point(226, 89);
            this.buttonInvertir.Name = "buttonInvertir";
            this.buttonInvertir.Size = new System.Drawing.Size(112, 33);
            this.buttonInvertir.TabIndex = 4;
            this.buttonInvertir.Text = "Invertir";
            this.buttonInvertir.UseVisualStyleBackColor = true;
            this.buttonInvertir.Click += new System.EventHandler(this.buttonInvertir_Click);
            // 
            // buttonMayus
            // 
            this.buttonMayus.Location = new System.Drawing.Point(344, 89);
            this.buttonMayus.Name = "buttonMayus";
            this.buttonMayus.Size = new System.Drawing.Size(109, 34);
            this.buttonMayus.TabIndex = 5;
            this.buttonMayus.Text = "Mayus";
            this.buttonMayus.UseVisualStyleBackColor = true;
            this.buttonMayus.Click += new System.EventHandler(this.buttonMayus_Click);
            // 
            // buttonMinusc
            // 
            this.buttonMinusc.Location = new System.Drawing.Point(459, 89);
            this.buttonMinusc.Name = "buttonMinusc";
            this.buttonMinusc.Size = new System.Drawing.Size(101, 33);
            this.buttonMinusc.TabIndex = 6;
            this.buttonMinusc.Text = "Minusculas";
            this.buttonMinusc.UseVisualStyleBackColor = true;
            this.buttonMinusc.Click += new System.EventHandler(this.buttonMinusc_Click);
            // 
            // buttonRotarD
            // 
            this.buttonRotarD.Location = new System.Drawing.Point(2, 127);
            this.buttonRotarD.Name = "buttonRotarD";
            this.buttonRotarD.Size = new System.Drawing.Size(105, 41);
            this.buttonRotarD.TabIndex = 7;
            this.buttonRotarD.Text = "Rotar D";
            this.buttonRotarD.UseVisualStyleBackColor = true;
            this.buttonRotarD.Click += new System.EventHandler(this.buttonRotarD_Click);
            // 
            // buttonRotarI
            // 
            this.buttonRotarI.Location = new System.Drawing.Point(113, 127);
            this.buttonRotarI.Name = "buttonRotarI";
            this.buttonRotarI.Size = new System.Drawing.Size(107, 41);
            this.buttonRotarI.TabIndex = 8;
            this.buttonRotarI.Text = "Rotar I";
            this.buttonRotarI.UseVisualStyleBackColor = true;
            this.buttonRotarI.Click += new System.EventHandler(this.buttonRotarI_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(587, 337);
            this.Controls.Add(this.buttonRotarI);
            this.Controls.Add(this.buttonRotarD);
            this.Controls.Add(this.buttonMinusc);
            this.Controls.Add(this.buttonMayus);
            this.Controls.Add(this.buttonInvertir);
            this.Controls.Add(this.buttonEscribir);
            this.Controls.Add(this.buttonInicializar);
            this.Controls.Add(this.labelResultado);
            this.Controls.Add(this.textBoxCadena);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxCadena;
        private System.Windows.Forms.Label labelResultado;
        private System.Windows.Forms.Button buttonInicializar;
        private System.Windows.Forms.Button buttonEscribir;
        private System.Windows.Forms.Button buttonInvertir;
        private System.Windows.Forms.Button buttonMayus;
        private System.Windows.Forms.Button buttonMinusc;
        private System.Windows.Forms.Button buttonRotarD;
        private System.Windows.Forms.Button buttonRotarI;
    }
}

