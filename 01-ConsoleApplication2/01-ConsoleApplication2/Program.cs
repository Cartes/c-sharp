﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            Type tipo;
            double numero;
            char letra;

            tipo = typeof(string);
            Console.WriteLine("El nombre corto es" + tipo.Name);
            Console.WriteLine("El nombre largo es" + tipo.FullName);

            //    Console.WriteLine("Hola mundo ");
            //    Console.ReadKey(); // si no ponemos esto nos cierra la ventana de una

            if ((3 + 4) is Int32)
                Console.WriteLine("Es entero");
            else
                Console.WriteLine("No es entero");

            numero = 32.9;
            Console.WriteLine("El numero es " + numero);
            Console.WriteLine("El numero convertido es " + (int)numero);

            Console.ReadKey();

            //Comprobar si una letra leida es una vocal
            letra = 'a';
            switch(letra)
            {
                case 'a': Console.WriteLine("Es vocal");
                    break;
                default: Console.WriteLine("Es asonante");
                    break;
            }

        }
    }
}
