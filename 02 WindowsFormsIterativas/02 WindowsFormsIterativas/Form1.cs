﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _02_WindowsFormsIterativas
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int numero;

            numero = Int32.Parse(textBoxNumero.Text);
            for(int i=1; i<=numero; i++)
            {
                if(i != numero)
                labelResultado.Text += i + " - ";
                else
                labelResultado.Text += i;
            }
        }

        private void buttonImprimirTabla_Click(object sender, EventArgs e)
        {

            int[] tabla = { 11, 22, 33, 44, 55 };
          /*
            //For tradicional
            for (int i = 0; i <tabla.Length; i++)
            {
                labelResultado.Text += tabla[i] +"|";
            }
            */
            //For solo colecciones, solo vale para recorrer desde el 0 hasta arriba .
            foreach(int numero in tabla)
            {
                labelResultado.Text += numero + "|";
            }
        
        }
    }
}
