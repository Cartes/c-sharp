﻿namespace _02_WindowsFormsIterativas
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonImprimir = new System.Windows.Forms.Button();
            this.textBoxNumero = new System.Windows.Forms.TextBox();
            this.labelResultado = new System.Windows.Forms.Label();
            this.buttonImprimirTabla = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonImprimir
            // 
            this.buttonImprimir.Location = new System.Drawing.Point(620, 128);
            this.buttonImprimir.Name = "buttonImprimir";
            this.buttonImprimir.Size = new System.Drawing.Size(191, 25);
            this.buttonImprimir.TabIndex = 0;
            this.buttonImprimir.Text = "Imprimir numeros";
            this.buttonImprimir.UseVisualStyleBackColor = true;
            this.buttonImprimir.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxNumero
            // 
            this.textBoxNumero.Location = new System.Drawing.Point(403, 129);
            this.textBoxNumero.Name = "textBoxNumero";
            this.textBoxNumero.Size = new System.Drawing.Size(172, 25);
            this.textBoxNumero.TabIndex = 1;
            // 
            // labelResultado
            // 
            this.labelResultado.AutoSize = true;
            this.labelResultado.Location = new System.Drawing.Point(410, 180);
            this.labelResultado.Name = "labelResultado";
            this.labelResultado.Size = new System.Drawing.Size(0, 17);
            this.labelResultado.TabIndex = 2;
            // 
            // buttonImprimirTabla
            // 
            this.buttonImprimirTabla.Location = new System.Drawing.Point(620, 159);
            this.buttonImprimirTabla.Name = "buttonImprimirTabla";
            this.buttonImprimirTabla.Size = new System.Drawing.Size(191, 25);
            this.buttonImprimirTabla.TabIndex = 0;
            this.buttonImprimirTabla.Text = "Imprimir Tabla";
            this.buttonImprimirTabla.UseVisualStyleBackColor = true;
            this.buttonImprimirTabla.Click += new System.EventHandler(this.buttonImprimirTabla_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Blue;
            this.ClientSize = new System.Drawing.Size(1157, 492);
            this.Controls.Add(this.labelResultado);
            this.Controls.Add(this.textBoxNumero);
            this.Controls.Add(this.buttonImprimirTabla);
            this.Controls.Add(this.buttonImprimir);
            this.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonImprimir;
        private System.Windows.Forms.TextBox textBoxNumero;
        private System.Windows.Forms.Label labelResultado;
        private System.Windows.Forms.Button buttonImprimirTabla;
    }
}

