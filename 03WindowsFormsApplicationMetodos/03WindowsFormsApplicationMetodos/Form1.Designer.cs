﻿namespace _03WindowsFormsApplicationMetodos
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSumar1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.labelResultado = new System.Windows.Forms.Label();
            this.button2Sumar2 = new System.Windows.Forms.Button();
            this.button3Sumar3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonSumar1
            // 
            this.buttonSumar1.Location = new System.Drawing.Point(595, 93);
            this.buttonSumar1.Name = "buttonSumar1";
            this.buttonSumar1.Size = new System.Drawing.Size(224, 42);
            this.buttonSumar1.TabIndex = 0;
            this.buttonSumar1.Text = "Sumar V1";
            this.buttonSumar1.UseVisualStyleBackColor = true;
            this.buttonSumar1.Click += new System.EventHandler(this.buttonSumar1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(269, 104);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(109, 20);
            this.textBox1.TabIndex = 1;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(412, 104);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(109, 20);
            this.textBox2.TabIndex = 1;
            // 
            // labelResultado
            // 
            this.labelResultado.AutoSize = true;
            this.labelResultado.Location = new System.Drawing.Point(276, 154);
            this.labelResultado.Name = "labelResultado";
            this.labelResultado.Size = new System.Drawing.Size(76, 13);
            this.labelResultado.TabIndex = 2;
            this.labelResultado.Text = "El resultado es";
            this.labelResultado.Click += new System.EventHandler(this.labelResultado_Click);
            // 
            // button2Sumar2
            // 
            this.button2Sumar2.Location = new System.Drawing.Point(599, 145);
            this.button2Sumar2.Name = "button2Sumar2";
            this.button2Sumar2.Size = new System.Drawing.Size(219, 42);
            this.button2Sumar2.TabIndex = 3;
            this.button2Sumar2.Text = "Sumar V2";
            this.button2Sumar2.UseVisualStyleBackColor = true;
            this.button2Sumar2.Click += new System.EventHandler(this.button2Sumar2_Click);
            // 
            // button3Sumar3
            // 
            this.button3Sumar3.Location = new System.Drawing.Point(599, 200);
            this.button3Sumar3.Name = "button3Sumar3";
            this.button3Sumar3.Size = new System.Drawing.Size(218, 41);
            this.button3Sumar3.TabIndex = 4;
            this.button3Sumar3.Text = "Sumar V3";
            this.button3Sumar3.UseVisualStyleBackColor = true;
            this.button3Sumar3.Click += new System.EventHandler(this.button3Sumar3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(899, 347);
            this.Controls.Add(this.button3Sumar3);
            this.Controls.Add(this.button2Sumar2);
            this.Controls.Add(this.labelResultado);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.buttonSumar1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSumar1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label labelResultado;
        private System.Windows.Forms.Button button2Sumar2;
        private System.Windows.Forms.Button button3Sumar3;
    }
}

