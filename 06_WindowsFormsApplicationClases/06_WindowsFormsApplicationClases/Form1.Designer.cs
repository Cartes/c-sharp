﻿namespace _06_WindowsFormsApplicationClases
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonA = new System.Windows.Forms.Button();
            this.labelResultado = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.labelResultado2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.labelResultado3 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.labelResultado4 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.labelResultado5 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.labelResultado6 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.labelResultado7 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.labelResultado8 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.labelResultado9 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.labelResultado10 = new System.Windows.Forms.Label();
            this.labelResultado11 = new System.Windows.Forms.Label();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.labelResultado12 = new System.Windows.Forms.Label();
            this.labelResultado13 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonA
            // 
            this.buttonA.Location = new System.Drawing.Point(31, 36);
            this.buttonA.Name = "buttonA";
            this.buttonA.Size = new System.Drawing.Size(130, 40);
            this.buttonA.TabIndex = 0;
            this.buttonA.Text = "Numeros_Aleatorios_10";
            this.buttonA.UseVisualStyleBackColor = true;
            this.buttonA.Click += new System.EventHandler(this.button1_Click);
            // 
            // labelResultado
            // 
            this.labelResultado.AutoSize = true;
            this.labelResultado.Location = new System.Drawing.Point(196, 50);
            this.labelResultado.Name = "labelResultado";
            this.labelResultado.Size = new System.Drawing.Size(79, 13);
            this.labelResultado.TabIndex = 1;
            this.labelResultado.Text = "El resultado es:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(31, 82);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(130, 40);
            this.button1.TabIndex = 2;
            this.button1.Text = "Numeros_Aleatorios_15";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // labelResultado2
            // 
            this.labelResultado2.AutoSize = true;
            this.labelResultado2.Location = new System.Drawing.Point(196, 96);
            this.labelResultado2.Name = "labelResultado2";
            this.labelResultado2.Size = new System.Drawing.Size(79, 13);
            this.labelResultado2.TabIndex = 3;
            this.labelResultado2.Text = "El resultado es:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(31, 128);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(130, 41);
            this.button2.TabIndex = 4;
            this.button2.Text = "Vaciar_Lista";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // labelResultado3
            // 
            this.labelResultado3.AutoSize = true;
            this.labelResultado3.Location = new System.Drawing.Point(196, 142);
            this.labelResultado3.Name = "labelResultado3";
            this.labelResultado3.Size = new System.Drawing.Size(46, 13);
            this.labelResultado3.TabIndex = 5;
            this.labelResultado3.Text = "Vacio10";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(31, 175);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(130, 41);
            this.button3.TabIndex = 6;
            this.button3.Text = "Vaciar_Lista_15";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // labelResultado4
            // 
            this.labelResultado4.AutoSize = true;
            this.labelResultado4.Location = new System.Drawing.Point(196, 189);
            this.labelResultado4.Name = "labelResultado4";
            this.labelResultado4.Size = new System.Drawing.Size(49, 13);
            this.labelResultado4.TabIndex = 7;
            this.labelResultado4.Text = "Vacio 15";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(33, 222);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(128, 35);
            this.button4.TabIndex = 8;
            this.button4.Text = "lista_15_inverso";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // labelResultado5
            // 
            this.labelResultado5.AutoSize = true;
            this.labelResultado5.Location = new System.Drawing.Point(196, 233);
            this.labelResultado5.Name = "labelResultado5";
            this.labelResultado5.Size = new System.Drawing.Size(76, 13);
            this.labelResultado5.TabIndex = 9;
            this.labelResultado5.Text = "El resultado es";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(33, 263);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(128, 31);
            this.button5.TabIndex = 10;
            this.button5.Text = "lista_numeros_Orden";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // labelResultado6
            // 
            this.labelResultado6.AutoSize = true;
            this.labelResultado6.Location = new System.Drawing.Point(196, 272);
            this.labelResultado6.Name = "labelResultado6";
            this.labelResultado6.Size = new System.Drawing.Size(97, 13);
            this.labelResultado6.TabIndex = 11;
            this.labelResultado6.Text = "Numeros en orden:";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(33, 300);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(128, 35);
            this.button6.TabIndex = 12;
            this.button6.Text = "lista_numeros_inverso";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // labelResultado7
            // 
            this.labelResultado7.AutoSize = true;
            this.labelResultado7.Location = new System.Drawing.Point(196, 311);
            this.labelResultado7.Name = "labelResultado7";
            this.labelResultado7.Size = new System.Drawing.Size(97, 13);
            this.labelResultado7.TabIndex = 13;
            this.labelResultado7.Text = "Numeros inversos: ";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(33, 341);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(128, 29);
            this.button7.TabIndex = 14;
            this.button7.Text = "Pares";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // labelResultado8
            // 
            this.labelResultado8.AutoSize = true;
            this.labelResultado8.Location = new System.Drawing.Point(196, 349);
            this.labelResultado8.Name = "labelResultado8";
            this.labelResultado8.Size = new System.Drawing.Size(79, 13);
            this.labelResultado8.TabIndex = 15;
            this.labelResultado8.Text = "Numeros Pares";
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(35, 376);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(126, 30);
            this.button8.TabIndex = 16;
            this.button8.Text = "Impares";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // labelResultado9
            // 
            this.labelResultado9.AutoSize = true;
            this.labelResultado9.Location = new System.Drawing.Point(196, 385);
            this.labelResultado9.Name = "labelResultado9";
            this.labelResultado9.Size = new System.Drawing.Size(89, 13);
            this.labelResultado9.TabIndex = 17;
            this.labelResultado9.Text = "Numeros Impares";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(35, 418);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(124, 27);
            this.button9.TabIndex = 18;
            this.button9.Text = "1 digito";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(35, 455);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(124, 28);
            this.button10.TabIndex = 19;
            this.button10.Text = "2 digitos";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // labelResultado10
            // 
            this.labelResultado10.AutoSize = true;
            this.labelResultado10.Location = new System.Drawing.Point(196, 425);
            this.labelResultado10.Name = "labelResultado10";
            this.labelResultado10.Size = new System.Drawing.Size(107, 13);
            this.labelResultado10.TabIndex = 20;
            this.labelResultado10.Text = "Numeros con 1 digito";
            // 
            // labelResultado11
            // 
            this.labelResultado11.AutoSize = true;
            this.labelResultado11.Location = new System.Drawing.Point(196, 466);
            this.labelResultado11.Name = "labelResultado11";
            this.labelResultado11.Size = new System.Drawing.Size(112, 13);
            this.labelResultado11.TabIndex = 21;
            this.labelResultado11.Text = "Numeros con 2 digitos";
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(35, 489);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(123, 32);
            this.button11.TabIndex = 22;
            this.button11.Text = "Coinciden";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(35, 527);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(122, 33);
            this.button12.TabIndex = 23;
            this.button12.Text = "No coinciden";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // labelResultado12
            // 
            this.labelResultado12.AutoSize = true;
            this.labelResultado12.Location = new System.Drawing.Point(196, 499);
            this.labelResultado12.Name = "labelResultado12";
            this.labelResultado12.Size = new System.Drawing.Size(119, 13);
            this.labelResultado12.TabIndex = 24;
            this.labelResultado12.Text = "Numeros que coinciden";
            // 
            // labelResultado13
            // 
            this.labelResultado13.AutoSize = true;
            this.labelResultado13.Location = new System.Drawing.Point(196, 537);
            this.labelResultado13.Name = "labelResultado13";
            this.labelResultado13.Size = new System.Drawing.Size(104, 13);
            this.labelResultado13.TabIndex = 25;
            this.labelResultado13.Text = "No existen en la lista";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(783, 582);
            this.Controls.Add(this.labelResultado13);
            this.Controls.Add(this.labelResultado12);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.labelResultado11);
            this.Controls.Add(this.labelResultado10);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.labelResultado9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.labelResultado8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.labelResultado7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.labelResultado6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.labelResultado5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.labelResultado4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.labelResultado3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.labelResultado2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.labelResultado);
            this.Controls.Add(this.buttonA);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonA;
        private System.Windows.Forms.Label labelResultado;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label labelResultado2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label labelResultado3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label labelResultado4;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label labelResultado5;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label labelResultado6;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label labelResultado7;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label labelResultado8;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label labelResultado9;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label labelResultado10;
        private System.Windows.Forms.Label labelResultado11;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Label labelResultado12;
        private System.Windows.Forms.Label labelResultado13;
    }
}

