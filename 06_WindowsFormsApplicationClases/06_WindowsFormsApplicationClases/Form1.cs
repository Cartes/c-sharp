﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _06_WindowsFormsApplicationClases

{
    public partial class Form1 : Form
    {
        //Atributos del formulario
        private ListaNumeros llamando, llamado2;
 
        public Form1()
        {
            InitializeComponent();
            llamando = new ListaNumeros();
            llamado2 = new ListaNumeros();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            labelResultado.Text = "";


            llamando = new ListaNumeros();//Si lo volvemos a declarar aqui, me aseguro de que se vacia cuando lo llamo
            for (int i = 0; i < 10; i++)
            { 
                int a = random.Next(0, 100); // valores de 0-99
                llamando.InsertarValor(a);
                String numero = (a.ToString());
                labelResultado.Text += (a + " ");
             }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Random random = new Random();

            labelResultado2.Text = "";

            llamado2 = new ListaNumeros();
            for (int i = 0; i < 15; i++)
            {
                int a = random.Next(0, 100);
                llamado2.InsertarValor(a);
                String numero = (a.ToString());
                labelResultado2.Text += (a + " ");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (int i=0; i<10; i++)
            {
                llamando.Vaciar();
                labelResultado.Text = "Se han vaciado los 10 numeros";
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 15; i++)
            {
                llamado2.Vaciar();
                labelResultado2.Text = "Se han vaciado los 15 numeros";
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //15,21,34,54,3,2,5,6,7,8,2
            //2,8,7,6,5,2,3,54,34,21,15
            int total = llamado2.NumeroElementos();

            for (int i = total - 1; i >= 0; i--)
            {
                int numero = llamado2.Elemento(i);
                labelResultado5.Text += numero.ToString() + "--";
            }        
        }

        private void button6_Click(object sender, EventArgs e)
        {

            int numeros = llamando.NumeroElementos();

            for(int i = numeros-1; i>=0; i--)
            {
                int devueltos = llamando.Sacar();
                labelResultado7.Text += devueltos.ToString() + "--";
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            int numeros = llamando.NumeroElementos();

            for (int i = 0; i < numeros; i++)
            {
                int devueltos = llamando.Sacar();
                labelResultado6.Text += devueltos.ToString() + "--";
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {

            int numeros = llamando.NumeroElementos();

            for (int i = 0; i < numeros; i++)
            {
                int devueltos = llamando.Sacar();
                if (devueltos % 2 == 0)
                    labelResultado8.Text += devueltos.ToString() + "--";
            }

        }

        private void button8_Click(object sender, EventArgs e)
        {
            int numeros = llamando.NumeroElementos();

            for (int i = 0; i < numeros; i++)
            {
                int devueltos = llamando.Sacar();
                if (devueltos % 2 == 1)
                    labelResultado9.Text += devueltos.ToString() + "--";
            }

        }

        private void button9_Click(object sender, EventArgs e)
        {

            int numeros = llamado2.NumeroElementos();

            for (int i = 0; i < numeros; i++)
            {
                int devueltos = llamando.Sacar();
                if (devueltos < 9)
                    labelResultado10.Text += devueltos.ToString() + "--";
            }

        }

        private void button10_Click(object sender, EventArgs e)
        {
            int numeros = llamado2.NumeroElementos();

            for (int i = 0; i < numeros; i++)
            {
                int devueltos = llamado2.Sacar();
                if (devueltos > 9)
                    labelResultado11.Text += devueltos.ToString() + "--";
            }

        }

        private void button11_Click(object sender, EventArgs e)
        {
            //Recorre la primera lista
            for (int i=0; i<llamando.NumeroElementos(); i++)
            {
                if (llamado2.Posicion(llamando.Elemento(i)) != -1)
                labelResultado12.Text += llamando.Elemento(i) + " ";
            }

            //Recorre la segunda lista
            for (int i = 0; i < llamado2.NumeroElementos(); i++)
            {
                if (llamado2.Posicion(llamado2.Elemento(i)) != -1)
                    labelResultado12.Text += llamado2.Elemento(i) + " ";
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            int numeros = llamado2.NumeroElementos();

        }


    }
}

