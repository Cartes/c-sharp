﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_WindowsFormsApplicationClases
{
    class ListaNumeros
    {
        private int[] nElementos = new int[100]; //inicialziar dentro de la funcion

        private int contadorElementos = 0;
        
        public ListaNumeros()
        {
            for (int i = 0; i<nElementos.Length; i++)
                nElementos[i] = 0;
            contadorElementos = 0;
        }

        public ListaNumeros(int repeticiones, int valor)
        {
            for (int i=0; i<repeticiones; i++)
            {
                nElementos[i] = valor;
            }
            contadorElementos = repeticiones;
        }

        public int NumeroElementos()
        {
            return contadorElementos;
        }

        public void InsertarValor(int valor)
        {
            if (contadorElementos < 100) {
            nElementos[contadorElementos] = valor;
            contadorElementos++;
        }
        }
        public int Primero()
        {   
           return nElementos[0];
        }

        public int Ultimo()
        {
            return nElementos[contadorElementos-1];
        }

        public int Sacar()
        {
            int valor = Primero();

            for (int i = 0; i < contadorElementos-1; i++)
            {
                nElementos[i] = nElementos[i+1];
            }
            contadorElementos--;

            return valor;
        }

        public int Elemento(int posicion)
        {
            int devuelve = 0;

            if (contadorElementos < posicion)
                return -10000;
             if (posicion>=1)
            devuelve = nElementos[posicion-1];

            return devuelve;
        }

        public int Posicion(int valor)
        {
            int comprueba = 0;


            for (int i=0; i<=contadorElementos; i++)
            {
                comprueba = nElementos[0];

                if (comprueba == valor)
                    return i;
            }
            return -1;
        }

        public Boolean Vacia()
        {
            if (contadorElementos == 0)
                return true;
            else
                return false;
        }

        public void Vaciar()
        {

        int[] nElementos = new int[100];
        int contadorElementos = 0;

    }
    }
}
